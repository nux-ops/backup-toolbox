#!/bin/bash
# export AWS_ACCESS_KEY_ID=$KEY_ID
# export AWS_SECRET_ACCESS_KEY=$ACCESS_KEY
# export AWS_DEFAULT_REGION=$Region
# export SQL_PASS=$SQL_PASS
MySqlHost="klio-mysql"
MySqlUser="root"
Grep="/bin/egrep"
Date="/bin/date"
Gzip="/bin/gzip"
Mysql="/usr/bin/mysql"
MysqlDump="/usr/bin/mysqldump"
MkDir="/bin/mkdir"
MysqlOpts="--user=${MySqlUser} --password=${SQL_PASS} --host=${MySqlHost} -N"
MysqlIgnoredDB="information_schema|performance_schema"
MysqlDumpOpts="--user=${MySqlUser} --password=${SQL_PASS} --host=${MySqlHost} --routines --triggers --add-drop-table --add-drop-database --create-options --complete-insert --events --databases"
DateCurrent=$(${Date} '+%Y-%m-%d')
BackupPath="${DateCurrent}"

if [ ! -d "${BackupPath}" ]; then
        $MkDir -p "${BackupPath}" && cd "${BackupPath}"
else
        echo "Error: ${BackupPath} already exists; exiting"
        exit 99
fi

for DB in $(echo "show databases;"|${Mysql} ${MysqlOpts}|${Grep} -v "${MysqlIgnoredDB}"); do
        ${MysqlDump} ${MysqlDumpOpts} ${DB} | ${Gzip} > "Mysql-${DB}-${DateCurrent}.sql.gz"
done

cd ../

aws s3 sync ${DateCurrent} s3://${MySqlHost}-backup/${DateCurrent}
